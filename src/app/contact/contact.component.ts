import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {error} from 'util';

const GoogleSpreadsheet = require('google-spreadsheet');
const creds = require('../../assets/sheet-api/credentials.json');


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html' ,
  styleUrls: ['./contact.component.css']
})

export class ContactComponent {
  constructor(private formBuilder: FormBuilder, private http: HttpClient, ) {

    this.contactForm = new FormGroup({
      fullName: new FormControl(),
      email: new FormControl(),
      message: new FormControl()
    });
  }

  contactForm: FormGroup;

  onSubmit() {
    const jsonObject = this.contactForm.value;
    console.log('Your form data : ', this.contactForm.value);
    const doc = new GoogleSpreadsheet('****YOUR SHREET ID HERE****');
    doc.useServiceAccountAuth(creds, function(err) {
      doc.addRow(1, jsonObject, function (err) {
        if (err) {
          console.log(err);
        }
      });
    });
  }

}
